package com.mobile.itworks.yunohh.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.mobile.itworks.yunohh.di.ActivityContext;
import com.mobile.itworks.yunohh.di.PerActivity;
import com.mobile.itworks.yunohh.ui.splash.SplashMvpPresenter;
import com.mobile.itworks.yunohh.ui.splash.SplashMvpView;
import com.mobile.itworks.yunohh.ui.splash.SplashPresenter;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class ActivityModule {

    private AppCompatActivity appCompatActivity;

    public ActivityModule(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return appCompatActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return appCompatActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    @PerActivity
    SplashMvpPresenter<SplashMvpView> provideSplashPresenter(SplashPresenter<SplashMvpView> splashPresenter) {
        return splashPresenter;
    }


}
