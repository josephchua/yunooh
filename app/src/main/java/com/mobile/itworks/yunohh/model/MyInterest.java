package com.mobile.itworks.yunohh.model;

public class MyInterest {

    private String iBannerUrl;
    private String iName;
    private String iStatus;
    private String iDistance;

    public String getiBannerUrl() {
        return iBannerUrl;
    }

    public void setiBannerUrl(String iBannerUrl) {
        this.iBannerUrl = iBannerUrl;
    }

    public String getiName() {
        return iName;
    }

    public void setiName(String iName) {
        this.iName = iName;
    }

    public String getiStatus() {
        return iStatus;
    }

    public void setiStatus(String iStatus) {
        this.iStatus = iStatus;
    }

    public String getiDistance() {
        return iDistance;
    }

    public void setiDistance(String iDistance) {
        this.iDistance = iDistance;
    }
}
