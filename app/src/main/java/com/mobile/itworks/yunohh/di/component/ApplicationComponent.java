package com.mobile.itworks.yunohh.di.component;

import android.app.Application;
import android.content.Context;

import com.mobile.itworks.yunohh.YunOhhApplication;
import com.mobile.itworks.yunohh.data.DataManager;
import com.mobile.itworks.yunohh.di.ApplicationContext;
import com.mobile.itworks.yunohh.di.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject (YunOhhApplication yunOhhApplication);

    @ApplicationContext
    Context context();

    Application application();

    DataManager getDatamanager();

}
