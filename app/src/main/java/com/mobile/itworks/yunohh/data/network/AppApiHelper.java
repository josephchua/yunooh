package com.mobile.itworks.yunohh.data.network;

import com.mobile.itworks.yunohh.AppConstants;
import com.mobile.itworks.yunohh.model.MyDashboard;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

@Singleton
public class AppApiHelper implements ApiHelper {

    @Inject
    public AppApiHelper() {
    }

    @Override
    public Single<MyDashboard> callMyDashboardApi() {
        return Rx2AndroidNetworking.get(AppConstants.myDashboardApi)
                .build()
                .getObjectSingle(MyDashboard.class);
    }
}
