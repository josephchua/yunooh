package com.mobile.itworks.yunohh.ui.base;

public interface MvpView {

    void showLoading();

    void hideLoading();

    void onError(String message);

    boolean isNetworkConnected();

}
