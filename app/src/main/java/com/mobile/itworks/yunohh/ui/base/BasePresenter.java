package com.mobile.itworks.yunohh.ui.base;

import com.mobile.itworks.yunohh.data.DataManager;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class BasePresenter<V extends MvpView> implements MvpPresenter<V> {

    private final CompositeDisposable compositeDisposable;
    private final DataManager dataManager;

    V mvpView;

    @Inject
    public BasePresenter(CompositeDisposable compositeDisposable, DataManager dataManager) {
        this.compositeDisposable = compositeDisposable;
        this.dataManager = dataManager;
    }


    @Override
    public void onAttach(V mvpView) {
        this.mvpView = mvpView;
    }

    @Override
    public void onDettach() {
        compositeDisposable.dispose();
        mvpView = null;
    }

    public CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    public DataManager getDataManager() {
        return dataManager;
    }

    public V getMvpView() {
        return mvpView;
    }
}
