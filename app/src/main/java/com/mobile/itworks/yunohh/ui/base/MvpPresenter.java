package com.mobile.itworks.yunohh.ui.base;

public interface MvpPresenter<V extends MvpView> {

    void onAttach(V mvpView);
    void onDettach();

}
