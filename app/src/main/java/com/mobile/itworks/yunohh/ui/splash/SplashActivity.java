package com.mobile.itworks.yunohh.ui.splash;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mobile.itworks.yunohh.R;
import com.mobile.itworks.yunohh.ui.base.BaseActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class SplashActivity extends BaseActivity implements SplashMvpView {

    @Inject
    SplashMvpPresenter<SplashMvpView> presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getActivityComponent().inject(this);

        ButterKnife.bind(this);

        presenter.loadNextActivity();

    }

}
