package com.mobile.itworks.yunohh.ui.splash;

import com.mobile.itworks.yunohh.di.PerActivity;
import com.mobile.itworks.yunohh.ui.base.MvpPresenter;

@PerActivity
public interface SplashMvpPresenter<V extends SplashMvpView> extends MvpPresenter<V> {

    void loadNextActivity();

}
