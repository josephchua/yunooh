package com.mobile.itworks.yunohh.di.module;

import android.app.Application;
import android.content.Context;

import com.mobile.itworks.yunohh.data.AppDataManager;
import com.mobile.itworks.yunohh.data.DataManager;
import com.mobile.itworks.yunohh.data.network.ApiHelper;
import com.mobile.itworks.yunohh.data.network.AppApiHelper;
import com.mobile.itworks.yunohh.di.ApplicationContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private final Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return application;
    }

    @Provides
    Application provideApplication() {
        return application;
    }

    @Provides
    @Singleton
    ApiHelper provideApiHelper(AppApiHelper appApiHelper) {
        return appApiHelper;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

}
