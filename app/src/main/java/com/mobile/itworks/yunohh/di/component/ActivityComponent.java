package com.mobile.itworks.yunohh.di.component;

import com.mobile.itworks.yunohh.di.PerActivity;
import com.mobile.itworks.yunohh.di.module.ActivityModule;
import com.mobile.itworks.yunohh.ui.splash.SplashActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject (SplashActivity splashActivity);

}
