package com.mobile.itworks.yunohh;

import android.app.Application;

import com.androidnetworking.AndroidNetworking;
import com.mobile.itworks.yunohh.di.component.ApplicationComponent;
import com.mobile.itworks.yunohh.di.component.DaggerApplicationComponent;
import com.mobile.itworks.yunohh.di.module.ApplicationModule;

public class YunOhhApplication extends Application {

    ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();

        applicationComponent.inject(this);

        AndroidNetworking.initialize(getApplicationContext());

    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

}
