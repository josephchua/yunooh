package com.mobile.itworks.yunohh.model;

import java.util.List;

public class MyDashboard {

    private String myAvatarUrl;
    private String myName;
    private String myLocation;
    private String myIdNumber;
    private String myInterestCount;
    private String myEarnPointsCount;
    private String myDealsCount;
    private List<MyInterest> myInterestList;

    public String getMyAvatarUrl() {
        return myAvatarUrl;
    }

    public void setMyAvatarUrl(String myAvatarUrl) {
        this.myAvatarUrl = myAvatarUrl;
    }

    public String getMyName() {
        return myName;
    }

    public void setMyName(String myName) {
        this.myName = myName;
    }

    public String getMyLocation() {
        return myLocation;
    }

    public void setMyLocation(String myLocation) {
        this.myLocation = myLocation;
    }

    public String getMyIdNumber() {
        return myIdNumber;
    }

    public void setMyIdNumber(String myIdNumber) {
        this.myIdNumber = myIdNumber;
    }

    public String getMyInterestCount() {
        return myInterestCount;
    }

    public void setMyInterestCount(String myInterestCount) {
        this.myInterestCount = myInterestCount;
    }

    public String getMyEarnPointsCount() {
        return myEarnPointsCount;
    }

    public void setMyEarnPointsCount(String myEarnPointsCount) {
        this.myEarnPointsCount = myEarnPointsCount;
    }

    public String getMyDealsCount() {
        return myDealsCount;
    }

    public void setMyDealsCount(String myDealsCount) {
        this.myDealsCount = myDealsCount;
    }

    public List<MyInterest> getMyInterestList() {
        return myInterestList;
    }

    public void setMyInterestList(List<MyInterest> myInterestList) {
        this.myInterestList = myInterestList;
    }
}
