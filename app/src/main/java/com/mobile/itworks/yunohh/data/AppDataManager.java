package com.mobile.itworks.yunohh.data;

import com.mobile.itworks.yunohh.data.network.ApiHelper;
import com.mobile.itworks.yunohh.model.MyDashboard;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

@Singleton
public class AppDataManager implements DataManager{

    private ApiHelper apiHelper;

    @Inject
    public AppDataManager(ApiHelper apiHelper) {
        this.apiHelper = apiHelper;
    }

    @Override
    public Single<MyDashboard> callMyDashboardApi() {
        return apiHelper.callMyDashboardApi();
    }
}
