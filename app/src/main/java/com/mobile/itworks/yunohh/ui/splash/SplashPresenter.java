package com.mobile.itworks.yunohh.ui.splash;

import com.mobile.itworks.yunohh.data.DataManager;
import com.mobile.itworks.yunohh.ui.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class SplashPresenter<V extends SplashMvpView> extends BasePresenter<V> implements SplashMvpPresenter<V> {

    @Inject
    public SplashPresenter(CompositeDisposable compositeDisposable, DataManager dataManager) {
        super(compositeDisposable, dataManager);
    }

    @Override
    public void loadNextActivity() {

    }
}
