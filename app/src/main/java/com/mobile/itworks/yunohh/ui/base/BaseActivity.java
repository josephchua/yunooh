package com.mobile.itworks.yunohh.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.mobile.itworks.yunohh.YunOhhApplication;
import com.mobile.itworks.yunohh.di.component.ActivityComponent;
import com.mobile.itworks.yunohh.di.component.DaggerActivityComponent;
import com.mobile.itworks.yunohh.di.module.ActivityModule;

import butterknife.Unbinder;

public class BaseActivity extends AppCompatActivity implements MvpView {

    ActivityComponent activityComponent;

    private Unbinder unbinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(((YunOhhApplication) getApplication()).getApplicationComponent())
                .build();

    }

    public ActivityComponent getActivityComponent() {
        return activityComponent;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public boolean isNetworkConnected() {
        return false;
    }

    public void setUnbinder(Unbinder unbinder) {
        this.unbinder = unbinder;
    }

    @Override
    protected void onDestroy() {

        if(unbinder != null) {
            unbinder.unbind();
        }

        super.onDestroy();
    }
}
