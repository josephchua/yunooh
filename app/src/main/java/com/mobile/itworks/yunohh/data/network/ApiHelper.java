package com.mobile.itworks.yunohh.data.network;

import com.mobile.itworks.yunohh.model.MyDashboard;

import io.reactivex.Single;

public interface ApiHelper {

    Single<MyDashboard> callMyDashboardApi();

}
